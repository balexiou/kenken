
> Difficulty level: Hard

> Solving file:  puzzle_hard_4x4.txt
> Algorithm: Backtrack FC Search
 ===Solution=== 
|1||4||3||2|
|2||1||4||3|
|4||3||2||1|
|3||2||1||4|

===Statistics===
> Elapsed time: 0.0110 seconds
> Number of Assignments:  218

> Solving file:  puzzle_hard_5x5.txt
> Algorithm: Backtrack FC Search
 ===Solution=== 
|1||2||3||4||5|
|2||1||4||5||3|
|3||4||5||2||1|
|4||5||1||3||2|
|5||3||2||1||4|

===Statistics===
> Elapsed time: 0.0050 seconds
> Number of Assignments:  47

> Solving file:  puzzle_hard_6x6.txt
> Algorithm: Backtrack FC Search
 ===Solution=== 
|2||5||1||4||6||3|
|6||4||2||1||3||5|
|1||2||4||3||5||6|
|5||3||6||2||1||4|
|3||1||5||6||4||2|
|4||6||3||5||2||1|

===Statistics===
> Elapsed time: 0.0140 seconds
> Number of Assignments:  109

> Solving file:  puzzle_hard_7x7.txt
> Algorithm: Backtrack FC Search
 ===Solution=== 