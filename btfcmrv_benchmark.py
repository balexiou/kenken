import sys
import time as tm
import pprint as pp
from kenken import *


easiest_difficulty_times = list()
easy_difficulty_times = list()
medium_difficulty_times = list()
hard_difficulty_times = list()
easiest_puzzles = ["easiest_0_3x3",
                   "easiest_1_3x3",
                   "easiest_4x4",
                   "easiest_5x5",
                   "easiest_6x6",
                   "easiest_7x7",
                   "easiest_8x8",
                   "easiest_9x9"]

easy_puzzles = ["easy_4x4",
                "easy_5x5",
                "easy_6x6",
                "easy_7x7",
                "easy_8x8",
                "easy_9x9"]

medium_puzzles = ["medium_4x4",
                  "medium_5x5",
                  "medium_6x6",
                  "medium_7x7",
                  "medium_8x8",
                  "medium_9x9"]

hard_puzzles = ["hard_4x4",
                "hard_5x5",
                "hard_6x6",
                "hard_7x7",
                "hard_8x8",
                "hard_9x9"]

difficulty = sys.argv[1]
if __name__ == "__main__":

    if difficulty == "easiest":
        easiest = 'puzzles\easiest'
        files_easiest = os.listdir(easiest)
        print('\n> Difficulty level: Easiest')
        for filename in files_easiest:
            print('\n> Solving file: ' , filename)
            print('> Algorithm: Backtrack FC+MRV Search')
            KenKen_puzzle = KenKen(filename)
            start_time = time.time()
            solution = backtracking_search(KenKen_puzzle, select_unassigned_variable=mrv, inference=forward_checking)
            termination_time = time.time()
            elapsed_time = termination_time - start_time
            easiest_difficulty_times.append(elapsed_time)
            KenKen_puzzle.kenken_display(solution)
            print("\n===Statistics===")
            print("> Elapsed time: %.4f seconds" % elapsed_time)
            print("> Number of Assignments: ", KenKen_puzzle.nassigns)
            del KenKen_puzzle

        easiest_zip = zip(easiest_puzzles, easiest_difficulty_times)
        easiest_benchmark = dict(easiest_zip)
        print('Easiest Difficulty Benchmark Times:')
        pp.pprint(easiest_benchmark)

    if difficulty == "easy":
        easy = 'puzzles\easy'
        files = os.listdir(easy)
        print('\n> Difficulty level: Easy')
        for filename in files:
            print('\n> Solving file: ' , filename)
            print('> Algorithm: Backtrack FC+MRV Search')
            KenKen_puzzle = KenKen(filename)
            start_time = time.time()
            solution = backtracking_search(KenKen_puzzle, select_unassigned_variable=mrv, inference=forward_checking)
            termination_time = time.time()
            elapsed_time = termination_time - start_time
            easy_difficulty_times.append(elapsed_time)
            KenKen_puzzle.kenken_display(solution)
            print("\n===Statistics===")
            print("> Elapsed time: %.4f seconds" % elapsed_time)
            print("> Number of Assignments: ", KenKen_puzzle.nassigns)
            del KenKen_puzzle
        easy_zip = zip(easy_puzzles, easy_difficulty_times)
        easy_benchmark = dict(easy_zip)
        print('Easy Difficulty Benchmark Times:')
        pp.pprint(easy_benchmark)

    if difficulty == "medium":
        medium = 'puzzles\medium'
        files = os.listdir(medium)
        print('\n> Difficulty level: Medium')
        for filename in files:
            print('\n> Solving file: ' , filename)
            print('> Algorithm: Backtrack FC+MRV Search')
            KenKen_puzzle = KenKen(filename)
            start_time = time.time()
            solution = backtracking_search(KenKen_puzzle, select_unassigned_variable=mrv, inference=forward_checking)
            termination_time = time.time()
            elapsed_time = termination_time - start_time
            medium_difficulty_times.append(elapsed_time)
            KenKen_puzzle.kenken_display(solution)
            print("\n===Statistics===")
            print("> Elapsed time: %.4f seconds" % elapsed_time)
            print("> Number of Assignments: ", KenKen_puzzle.nassigns)
            del KenKen_puzzle
        medium_zip = zip(medium_puzzles, medium_difficulty_times)
        medium_benchmark = dict(medium_zip)
        print('Medium Difficulty Benchmark Times:')
        pp.pprint(medium_benchmark)

    if difficulty == "hard":
        hard = 'puzzles\hard'
        files = os.listdir(hard)
        print('\n> Difficulty level: Hard')
        for filename in files:
            print('\n> Solving file: ' , filename)
            print('> Algorithm: Backtrack FC+MRV Search')
            KenKen_puzzle = KenKen(filename)
            start_time = time.time()
            solution = backtracking_search(KenKen_puzzle, select_unassigned_variable=mrv, inference=forward_checking)
            termination_time = time.time()
            elapsed_time = termination_time - start_time
            hard_difficulty_times.append(elapsed_time)
            KenKen_puzzle.kenken_display(solution)
            print("\n===Statistics===")
            print("> Elapsed time: %.4f seconds" % elapsed_time)
            print("> Number of Assignments: ", KenKen_puzzle.nassigns)
            del KenKen_puzzle

        hard_zip = zip(hard_puzzles, hard_difficulty_times)
        hard_benchmark = dict(hard_zip)
        print('Hard Difficulty Benchmark Times:')
        pp.pprint(hard_benchmark)
