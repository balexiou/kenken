import os
import ast
import time
import pprint as pp
from csp import *
from timeit import Timer

class KenKen(CSP):

    def __init__(self, puzzle_layout_file):
        self.dimensions = None
        self.variables = list()
        self.domains = defaultdict(list)
        self.cage = defaultdict(list)
        self.adjacent_cages = defaultdict(list)
        self.neighbors = defaultdict(list)


        with open(puzzle_layout_file) as puzzle:
            lines = puzzle.readlines()

        self.dimensions = int(lines[0])

        # * Variables extraction from the puzzle layout file
        for line in lines[1:]:
            tokens = line.split()
            tokens[2] = ast.literal_eval(tokens[2])
            for coordinate_variables in tokens[2]:
                self.variables.append(coordinate_variables)
        # * Cages dict
        for cage_number, line in enumerate(lines[1:]):
            tokens = line.split()
            target_value = int(tokens[0])
            operation = tokens[1]
            coordinates_list = ast.literal_eval(tokens[2])
            for coordinates in coordinates_list:
                self.cage[coordinates].append((cage_number, target_value, operation))

        for coordinates in self.variables:
            domain = range(1, self.dimensions + 1)
            self.domains[coordinates] = list(domain)

        for coordinates in self.variables:
            for x in range(self.dimensions):
                for y in range(self.dimensions):
                    if x == coordinates[0] and y != coordinates[1]:
                        self.neighbors[coordinates].append((x,y))
                    elif x != coordinates[0] and y == coordinates[1]:
                        self.neighbors[coordinates].append((x,y))

        for line in lines[1:]:
            tokens = line.split()
            coordinates_list = ast.literal_eval(tokens[2])
            for coordinate_variables in coordinates_list:
                for adjacent_coordinates in coordinates_list:
                    if coordinate_variables != adjacent_coordinates:
                        self.adjacent_cages[coordinate_variables].append(adjacent_coordinates)

        CSP.__init__(self, variables=self.variables, domains=self.domains, neighbors=self.neighbors, constraints=self.kenken_constraints)

        # print("Dimensions: {}x{}".format(self.dimensions, self.dimensions))
        # print("Variables:")
        # pp.pprint(sorted(self.variables))
        # print("Domain:")
        # pp.pprint(dict(self.domains))
        # print("Neighbors:")
        # pp.pprint(dict(self.neighbors))
        # print("Cages:")
        # pp.pprint(dict(self.cage))

    def arithmetic_constraints(self, A, a, B, b):
        cage_A_value = a
        cage_B_value = b
        cage_operation = self.cage[A][0][2]
        cage_to_assign = self.infer_assignment()
        cage_target_value = self.cage[A][0][1]
        assigned_cage_idx = 0

        if cage_operation == 'Add':
            summation_value = cage_A_value
            for cage in self.adjacent_cages[A]:
                if cage in cage_to_assign and cage != B:
                    summation_value += cage_to_assign[cage]
                    assigned_cage_idx += 1
            summation_value += cage_B_value
            all_block_cages_assigned = (assigned_cage_idx == len(self.adjacent_cages[A]) - 1)
            block_cages_partially_assigned = (assigned_cage_idx < len(self.adjacent_cages[A]) - 1)
            summation_value_is_target_value = (summation_value == cage_target_value)
            summation_value_not_target_value = (summation_value < cage_target_value)

            if all_block_cages_assigned:
                return summation_value_is_target_value
            elif block_cages_partially_assigned:
                return summation_value_not_target_value
            else:
                return False

        if cage_operation == 'Sub':
            difference_value = abs(a - b)
            if difference_value == cage_target_value:
                return True
            else:
                return False

        if cage_operation == 'Mul':
            product_value = cage_A_value
            for cage in self.adjacent_cages[A]:
                if cage in cage_to_assign and cage != B:
                    assigned_cage_idx += 1
                    product_value *= cage_to_assign[cage]
            product_value *= cage_B_value
            all_block_cages_assigned = (assigned_cage_idx == len(self.adjacent_cages[A]) - 1)
            block_cages_partially_assigned = (assigned_cage_idx < len(self.adjacent_cages[A]) - 1)
            product_value_is_target_value = (product_value == cage_target_value)
            product_value_not_target_value = (product_value < cage_target_value)

            if all_block_cages_assigned:
                return product_value_is_target_value
            elif block_cages_partially_assigned:
                return product_value_not_target_value
            else:
                return False

        if cage_operation == 'Div':
            numerator = max(a, b)
            denominator = min(a, b)
            quotient_value = numerator / denominator
            quotient_value_is_target_value = (quotient_value == cage_target_value)
            return quotient_value_is_target_value

        if cage_operation == 'Freebie':
            freebie = a
            is_target_value = (freebie == cage_target_value)
            return is_target_value

    def all_different_values(self, A, a, B, b):
        return a != b

    def is_same_block(self, A, a, B, b):
        if self.cage[A][0] == self.cage[B][0]:
            return self.arithmetic_constraints(A, a, B, b)
        else:
            return True

    def kenken_constraints(self, A, a, B, b):
        all_different = self.all_different_values(A, a, B, b)
        same_block = self.is_same_block(A, a, B, b)
        return (same_block and all_different)

    def kenken_display(self,assignment):
        print(' ===Solution=== ')
        for length_idx in range(self.dimensions):
            for coordinates, target_value in sorted(assignment.items()):
                if coordinates[0] == length_idx:
                    print("|" + str(target_value), end="|")
            print('\n',end="")



if __name__ == "__main__":
    KenKen = KenKen('puzzle_medium_9x9.txt')
    solution = backtracking_search(KenKen, inference=forward_checking, select_unassigned_variable=mrv)
    print(KenKen.nassigns)
    KenKen.kenken_display(solution)

