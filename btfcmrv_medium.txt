
> Difficulty level: Medium

> Solving file:  puzzle_medium_4x4.txt
> Algorithm: Backtrack FC+MRV Search
 ===Solution=== 
|2||4||1||3|
|1||3||2||4|
|4||1||3||2|
|3||2||4||1|

===Statistics===
> Elapsed time: 0.0000 seconds
> Number of Assignments:  93

> Solving file:  puzzle_medium_5x5.txt
> Algorithm: Backtrack FC+MRV Search
 ===Solution=== 
|5||1||4||3||2|
|4||5||2||1||3|
|3||2||5||4||1|
|1||4||3||2||5|
|2||3||1||5||4|

===Statistics===
> Elapsed time: 0.0156 seconds
> Number of Assignments:  154

> Solving file:  puzzle_medium_6x6.txt
> Algorithm: Backtrack FC+MRV Search
 ===Solution=== 
|1||3||2||4||6||5|
|2||1||5||6||3||4|
|4||2||1||3||5||6|
|3||6||4||5||2||1|
|5||4||6||2||1||3|
|6||5||3||1||4||2|

===Statistics===
> Elapsed time: 0.2879 seconds
> Number of Assignments:  1977

> Solving file:  puzzle_medium_7x7.txt
> Algorithm: Backtrack FC+MRV Search
 ===Solution=== 
|3||6||5||4||2||1||7|
|7||1||3||6||5||2||4|
|5||3||2||1||7||4||6|
|2||5||6||7||4||3||1|
|4||2||7||3||1||6||5|
|1||7||4||2||6||5||3|
|6||4||1||5||3||7||2|

===Statistics===
> Elapsed time: 4.2338 seconds
> Number of Assignments:  28785

> Solving file:  puzzle_medium_8x8.txt
> Algorithm: Backtrack FC+MRV Search
 ===Solution=== 
|4||3||5||6||1||8||7||2|
|2||8||7||5||6||3||4||1|
|1||4||2||8||5||6||3||7|
|6||7||4||2||8||1||5||3|
|3||1||6||4||7||5||2||8|
|7||5||8||1||3||2||6||4|
|5||2||1||3||4||7||8||6|
|8||6||3||7||2||4||1||5|

===Statistics===
> Elapsed time: 40.8580 seconds
> Number of Assignments:  258115

> Solving file:  puzzle_medium_9x9.txt
> Algorithm: Backtrack FC+MRV Search
 ===Solution=== 

===Statistics===
> Elapsed time: 
> Number of Assignments:  